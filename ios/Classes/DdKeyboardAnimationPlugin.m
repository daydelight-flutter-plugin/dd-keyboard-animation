#import "DdKeyboardAnimationPlugin.h"
#if __has_include(<dd_keyboard_animation/dd_keyboard_animation-Swift.h>)
#import <dd_keyboard_animation/dd_keyboard_animation-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "dd_keyboard_animation-Swift.h"
#endif

@implementation DdKeyboardAnimationPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftDdKeyboardAnimationPlugin registerWithRegistrar:registrar];
}
@end
