import 'package:dd_keyboard_animation/dd_keyboard_animation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: _buildBody(),
      ),
    );
  }

  _buildBody() {
    return DDFooterLayout(
      footer: DDKeyboardAttachable(
        child: CupertinoTextField(),
      ),
      child: ListView(
        reverse: true,
        children: [
          ...List.generate(50, (index) => index)
              .map(
                (e) => ListTile(
                  title: Text("This is title"),
                  subtitle: Text("This is content"),
                  leading: Text("$e"),
                ),
              )
              .toList()
        ],
      ),
    );
  }
}
